bash "install_dateutil3" do
  user "root"
  cwd "/tmp"
  code <<-EOH
  wget https://pypi.python.org/packages/source/p/python-dateutil/python-dateutil-2.2.tar.gz#md5=c1f654d0ff7e33999380a8ba9783fd5c
  tar -zxf python-dateutil-2.2.tar.gz 
  cd python-dateutil-2.2
  python3 ./setup.py install
  EOH
end
