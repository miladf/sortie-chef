git node[:sortie_server][:download_path] do
  repository node[:sortie_server][:repo_url]
  reference "master"
  action :sync
end

bash "Installing archiver" do
  cwd "#{node[:sortie_server][:download_path]}/archiver"
  code <<-END_CODE  
  ./setup.py install
  END_CODE
end

bash "Installing archiver.conf to /etc/init" do
  code <<-END_CODE  
  cp #{node[:sortie_server][:download_path]}/archiver/config/archiver.conf /etc/init
  END_CODE
end

user "archiver" do
  comment "user for sortie archiver program"
  system true
  shell "/bin/false"
end

service "archiver" do
  provider Chef::Provider::Service::Upstart
  action :restart
end

bash "Installing web application" do
  cwd "#{node[:sortie_server][:download_path]}/webpy"
  code <<-END_CODE  
 
  mkdir /var/www/sortie
  mkdir /var/www/sortie/static

  cp httpd.conf /etc/apache2
  cp sortie.py /var/www/sortie
  cp sortieconf.py /var/www/sortie

  END_CODE
end
